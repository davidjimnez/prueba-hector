import React, { Component } from 'react';
import { Row, Col, Button, InputNumber, Form } from 'antd';

import { connect } from "react-redux";

class BotonInput extends Component {
	render() {
        const { fetchingBotonInput, infoInput, onMandarInput } = this.props;
        console.log(fetchingBotonInput, infoInput);
        const { getFieldDecorator } = this.props.form;

        const mandarNumero = (e) => {
            e.preventDefault();
            this.props.form.validateFields((err, formulario) => {
				if (err) {
					return;
                }
                console.log(formulario)
                onMandarInput(formulario);
			});
        }
		return (
            <div>
                
                <Form onSubmit={mandarNumero}>
                    <Row type="flex" justify="space-around">
                        <Col xs={24} sm={6}>
                        <h1>PRUEBA</h1>
                            <Form.Item label="Introduzca un número: ">
                                {getFieldDecorator('numero',{
                                    rules: [{ required: true, message: 'Favor de llenar el campo' }],
                                })(
                                    <InputNumber style={{width: '100%'}}/>
                                )}
                            </Form.Item>
                            <Button loading={fetchingBotonInput} type="primary" htmlType="submit" size="large" style={{width: '100%'}}>Mandar número</Button> 
                        </Col>
                        <Col xs={24} sm={6}>
                            <h1>{infoInput}</h1>
                        </Col>
                    </Row>
                </Form>
            </div>
		);
	}
}

const mapStateToProps = state => {
	return {
        fetchingBotonInput: state.DashboardReducer.fetchingBotonInput,
        infoInput: state.DashboardReducer.infoInput,
	};
};

const mapDispatchToProps = dispatch => {
    return {
        onMandarInput: (formulario) => {
            dispatch({type: "BOTON_INPUT_REQUEST", formulario: formulario});
        },
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(Form.create()(BotonInput));