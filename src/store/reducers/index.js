//Archivo que junta todos los reducers de la aplicacion
import { combineReducers } from "redux";

import DashboardReducer from "./DashboardReducer";

export default combineReducers({DashboardReducer});