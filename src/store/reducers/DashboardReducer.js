import * as dashboardActions from '../actions/DashboardActions'; 

//Variables disponibles del STATE que se usan en el dashboard 
const initialState = {
    fetchingBotonInput:false,
    infoInput: 'No hay número seleccionado',
};

export default(state = initialState, action) => {
    switch (action.type) {

        case dashboardActions.BOTON_INPUT_REQUEST:
            return {...state, fetchingBotonInput: true};
        case dashboardActions.BOTON_INPUT_SUCCESS:
            return {...state, fetchingBotonInput: false, infoInput: action.infoInput};
        case dashboardActions.BOTON_INPUT_FAILURE:
            return {...state, fetchingBotonInput: false};

        default:
            return state;
    }
};