//Archivo que une todos los archivos Saga
import { all, fork } from "redux-saga/effects";

import * as DashboardSaga from "./DashboardSaga";

export default function* rootSaga() {

var allSagas = Object.assign(DashboardSaga);

  yield all(
    [...Object.values(allSagas)].map(fork)
  );
}