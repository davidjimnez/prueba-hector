import { takeLatest, call, put } from "redux-saga/effects";
import * as dashboardActions from '../actions/DashboardActions';

function* botonInput(action) {
    try {
        console.log(action);
        let infoInput = 'Número ingresado: ' + action.formulario.numero;
        yield put({type: dashboardActions.BOTON_INPUT_SUCCESS, infoInput});

    } catch (error) {
        console.log(error);
        yield put({type: dashboardActions.BOTON_INPUT_FAILURE});
    }
}
export function* botonInputSaga() {
    yield takeLatest(dashboardActions.BOTON_INPUT_REQUEST, botonInput);
}