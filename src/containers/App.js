import React, {Component} from 'react';
import DashboardContainer from './DashboardContainer';
import './App.css';

import {
	BrowserRouter as Router,
	Route,
	Switch
} from 'react-router-dom';

class App extends Component{
	render() {
		return (
			<div>
				<Router>
					<Switch>
						<Route exact path="/" component={DashboardContainer} />
					</Switch>
				</Router>
			</div>
		);
	}
}

export default App;
