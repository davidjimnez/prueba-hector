import React, { Component } from 'react';
import '../stylesheets/Body.css';
import BotonInput from '../components/BotonInput';

class DashboardContainer extends Component {
	render() {
		return (
            <div className="containerDashboard">
                <BotonInput/>
            </div>
		);
	}
}

export default DashboardContainer;